<?php
// $Id$

function regonline_reports_overview() {
  $out = '';
  // Count of registrations.
  $result = db_fetch_array(db_query("SELECT COUNT(regid) AS count FROM {regonline_registrants}"));
  $registration_count = $result['count'];

  // Count of mapped registrations.
  $result = db_fetch_array(db_query("SELECT COUNT(regid) AS count FROM {regonline_registrants_map} WHERE uid IS NOT NULL OR duid IS NOT NULL"));
  $map_count = $result['count'];
  
  // Show mapped registrants.
  $out .= t('<p><strong>!count total registrants</strong></p>', array('!count' => $registration_count));
  $out .= t('<h3>Registrants with associated Drupal accounts</h3>', array('!count' => $registration_count));
  $out .= views_embed_view('regonline_mapped_registrants');
  // Show unmapped.
  $out .= t('<h3>!count Registrants with no associated Drupal accounts</h3>', array('!count' => $registration_count - $map_count));
  $out .= views_embed_view('regonline_unmapped_registrants');
  
  return $out;
}

function regonline_reports_unmapped() {
  return views_embed_view('unmapped_registrants');
}

function regonline_reports_add_map() {
  if (!empty($_GET['regid'])) {
    // Retrieve info about this registrant.
    $result = db_query("SELECT firstlastname, email_address, ccname FROM {regonline_registrants} WHERE regid = %d", $_GET['regid']);
    if ($result) {
      $record = db_fetch_array($result);
      if (!empty($record)) {
        $form['registrant_info'] = array(
          '#type' => 'markup',
          '#value' => t('<strong>Registrant @regid RegOnline info</strong>: @email, @name, @cc', array('@regid' => $_GET['regid'], '@email' => $record['email_address'], '@name' => $record['firstlastname'], '@cc' => $record['ccname'])),
        );
      }
    }
  }
  $form['regid'] = array(
    '#type' => 'textfield',
    '#title' => t('RegOnline RegID'),
    '#required' => TRUE,
    '#description' => t("The RegOnline's registrant ID number."),
    '#default_value' => !empty($_GET['regid']) ? $_GET['regid'] : '',
  );
  $form['uid'] = array(
    '#type' => 'textfield',
    '#title' => t('Chicago2011 Username/UID'),
    '#autocomplete_path' => 'user/autocomplete',
    '#description' => t("The local chicago2011.drupal.org user ID or username. Only required if drupal.org user ID is missing."),
  );
  $form['duid'] = array(
    '#type' => 'textfield',
    '#title' => t('drupal.org UID'),
    '#description' => t("The drupal.org user ID. Only required if chicago2011.drupal.org user ID is missing."),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

function regonline_reports_add_map_validate($form, &$form_state) {
  if (empty($form_state['values']['uid']) && empty($form_state['values']['duid'])) {
    form_set_error('uid', t('You must enter at least one of the UID fields.'));
  }
  else {
    if (isset($form_state['values']['uid']) && !is_numeric($form_state['values']['uid'])) {
      // A username was submitted, map it to UID because we store UIDs.
      $account = user_load(array('name' => $form_state['values']['uid']));
      if (!$account) {
        form_set_error('uid', t('Username not found.'));
      }
      else {
        $form_state['values']['uid'] = $account->uid;
      }
    }
    // Check if any values being submitted already exist.
    $record = array(
      'regid' => $form_state['values']['regid'],
      'uid' => $form_state['values']['uid'] ? $form_state['values']['uid'] : -1, // -1 avoids matching NULL records.
      'duid' => $form_state['values']['duid'] ? $form_state['values']['duid'] : -1, // -1 avoids matching NULL records.
    );
    $result = db_query("SELECT regid FROM {regonline_registrants_map} WHERE regid = %d OR uid = %d OR duid = %d", $record);
    if ($result && db_fetch_array($result)) {
      form_set_error('regid', t('A record already exists for one of these fields.'));
    }
  }
}

function regonline_reports_add_map_submit($form, &$form_state) {
  $record = array(
    'regid' => $form_state['values']['regid'],
    'uid' => $form_state['values']['uid'] ? $form_state['values']['uid'] : NULL,
    'duid' => $form_state['values']['duid'] ? $form_state['values']['duid'] : NULL,
  );
  drupal_write_record('regonline_registrants_map', $record);
  // Inform other modules about registrant association.
  $registrant = db_fetch_object(db_query("SELECT * FROM {regonline_registrants} WHERE regid = %d", $record['regid']));
  $registrant->map = $record;
  $registrants = array($registrant);
  module_invoke_all('regonline_registrants_update', $registrants, $registrants);

  drupal_set_message(t('Association map created'));
  drupal_goto('admin/reports/regonline');
}

function regonline_reports_edit_map($form, $regid) {
  $registrant = db_fetch_array(db_query("SELECT regid, uid, duid FROM {regonline_registrants_map} WHERE regid = %d", $regid));
  if ($registrant) {
    $form = regonline_reports_add_map();
    $form['regid']['#default_value'] = $registrant['regid'];
    $form['uid']['#default_value'] = $registrant['uid'];
    $form['duid']['#default_value'] = $registrant['duid'];
    
    return $form;
  }
  else {
    drupal_set_message(t('An association to registrant with id @id does not exist', array('@id' => $regid)), 'error');
    drupal_goto('admin/reports/regonline');
  }
}

function regonline_reports_edit_map_validate($form, &$form_state) {
  if (empty($form_state['values']['uid']) && empty($form_state['values']['duid'])) {
    form_set_error('uid', t('You must enter at least one of the UID fields.'));
  }
}

function regonline_reports_edit_map_submit($form, &$form_state) {
  $record = array(
    'regid' => $form_state['values']['regid'],
    'uid' => $form_state['values']['uid'] ? $form_state['values']['uid'] : NULL,
    'duid' => $form_state['values']['duid'] ? $form_state['values']['duid'] : NULL,
  );
  drupal_write_record('regonline_registrants_map', $record, 'regid');
  drupal_set_message(t('Association map updated'));
  drupal_goto('admin/reports/regonline');
}

/**
 * Broadcast email form.
 */
function regonline_reports_broadcast() {
  // Count of registrations.
  $result = db_fetch_array(db_query("SELECT COUNT(regid) AS count FROM {regonline_registrants}"));
  $registration_count = $result['count'];

  // Count of mapped registrations.
  $result = db_fetch_array(db_query("SELECT COUNT(regid) AS count FROM {regonline_registrants_map} WHERE uid IS NOT NULL OR duid IS NOT NULL"));
  $count = $registration_count - $result['count'];

  $form['desc'] = array(
    '#type' => 'markup',
    '#value' => t('<h3>Broadcast email self-map steps to !count unmapped registrants?</h3>', array('!count' => $count)),
  );
  $enabled = variable_get('regonline_reports_self_enabled', FALSE);
  if (!$enabled) {
    drupal_set_message(t('Broadcast is disabled, enable it on the Settings page to continue'), 'warning');
  }
  
  $form['regonline_reports_broadcast_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#disabled' => $enabled ? FALSE : TRUE,
    '#default_value' => variable_get('regonline_reports_broadcast_subject', t('Action required to finalize your DrupalCon registration')), 
    '#maxlength' => 180,
  );
  $form['regonline_reports_broadcast_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#disabled' => $enabled ? FALSE : TRUE,
    '#default_value' => variable_get('regonline_reports_broadcast_body', t("Dear !fullname,\n\nYour DrupalCon registration is nearly complete and requires one small step to finalize. Please click the link below to visit the DrupalCon Chicago site and finalize your account.\n\n!link\n\nYou will need to be logged into the DrupalCon site to finalize your account, so after logging in you can copy the link into your browser and visit.\n\nThank you,\nDrupalCon Chicago team")),
    '#description' => t('Tokens: <em>!fullname</em> for full RegOnline registrant name, <em>!link</em> for link to self map URL (required).'),
    '#rows' => 15,
  );
  $form['count'] = array(
    '#type' => 'value',
    '#value' => $count,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#disabled' => $enabled ? FALSE : TRUE,
    '#value' => t('Send emails'),
  );
  $form['cancel'] = array(
    '#type' => 'markup',
    '#value' => l(t('Cancel'), 'admin/reports/regonline'),
  );

  return $form;
}

function regonline_reports_broadcast_submit($form, &$form_state) {
  global $base_url;
  // Set text variables.
  variable_set('regonline_reports_broadcast_subject', $form_state['values']['regonline_reports_broadcast_subject']);
  variable_set('regonline_reports_broadcast_body', $form_state['values']['regonline_reports_broadcast_body']);

  // Send emails.
  if ($form_state['values']['count'] > 30) {
    // Use batch to send.
    $batch = array(
      'title' => t('Sending'),
      'operations' => array(
        array('_regonline_reports_batch_op', array()),
      ),
      'finished' => '_regonline_reports_batch_fin',
      'file' => drupal_get_path('module', 'regonline_reports') . '/regonline_reports.admin.inc',
    );
    batch_set($batch);
  }
  else {
    // Loop and send.
    $records = array();
    $result = db_query("SELECT regid, firstlastname, email_address, regdate FROM {regonline_registrants} WHERE regid NOT IN (SELECT regid FROM {regonline_registrants_map})");
    while ($record = db_fetch_object($result)) {
      $records[] = $record;
    }
    if (!empty($records)) {
      foreach ($records as $record) {
        // Generate link.
        $link = $base_url . '/regonlineregmap/' . $record->regid . '/' . _regonline_reports_registrant_hash($record);
        $params = array(
          'registrant' => $record,
          'link' => $link,
        );
        drupal_mail('regonline_reports', 'broadcast', $record->email_address, language_default(), $params, NULL, TRUE);
      }
    }
  }
}

/**
 * Batch broadcast operation.
 */
function _regonline_reports_batch_op(&$context) {
  global $base_url;

  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['max'] = db_result(db_query("SELECT COUNT(regid) FROM {regonline_registrants} WHERE regid NOT IN (SELECT regid FROM {regonline_registrants_map})"));
  }
  $limit = 30;
  $result = db_query_range("SELECT regid, firstlastname, email_address, regdate FROM {regonline_registrants} WHERE regid NOT IN (SELECT regid FROM {regonline_registrants_map}) AND regid > %d ORDER BY regid ASC", $context['sandbox']['current'], 0, $limit);
  while ($record = db_fetch_object($result)) {
    // Generate link.
    $link = $base_url . '/regonlineregmap/' . $record->regid . '/' . _regonline_reports_registrant_hash($record);
    $params = array(
      'registrant' => $record,
      'link' => $link,
    );
    drupal_mail('regonline_reports', 'broadcast', $record->email_address, language_default(), $params, NULL, TRUE);

    $context['sandbox']['progress']++;
    $context['sandbox']['current'] = $record->regid;
    $context['results'][] = $record->regid;
    $context['message'] = t('Sent !count out of !max', array('!count' => $context['sandbox']['progress'], '!max' => $context['sandbox']['max']));
  }

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Finished callback for Batch broadcast operation.
 */
function _regonline_reports_batch_fin($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), 'Success, one email sent.', 'Success, @count emails sent.');
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
}

/**
 * Implementation of hook_mail().
 */
function regonline_reports_mail($key, &$message, $params) {
  if ($key == 'broadcast') {
    $registrant = $params['registrant'];
    $variables = array(
      '!link' => $params['link'],
      '!fullname' => $registrant->firstlastname,
    );
    $message['subject'] = strtr(variable_get('regonline_reports_broadcast_subject', ''), $variables);
    $message['body'] = strtr(variable_get('regonline_reports_broadcast_body', ''), $variables);
  }
}

function regonline_reports_settings() {
  $form['regonline_reports_ignore'] = array(
    '#type' => 'textarea',
    '#title' => t('Ignored RegOnline Registrant IDs'),
    '#description' => t('Separate IDs by a new-line. Some circumstances require a registrant to cancel and re-register, resulting in duplicate RegOnline registrant entries for the same person (perhaps under different email addresses). Because it is difficult to systematically understand these circumstances you can use this field to enter RegOnline IDs that should not be processed from retrieved reports.'),
    '#default_value' => variable_get('regonline_reports_ignore', array()),
  );
  $form['regonline_reports_self_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable self registrant mapping'),
    '#description' => t('Enable broadcast and for the ability for a user to follow through and associate their RegOnline account.'),
    '#default_value' => variable_get('regonline_reports_self_enabled', FALSE),
  );
  $form['selfmap_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Finalize registration page text'),
    '#collapsible' => TRUE,
  );
  $form['selfmap_fieldset']['regonline_reports_self_invalid'] = array(
    '#type' => 'textarea',
    '#title' => t('Invalid attempt'),
    '#description' => t('If the provided RegID or hash value are not valid the user will not be allowed to continue and should try again.'),
    '#default_value' => variable_get('regonline_reports_self_invalid', t('There was an error in trying to finalize your registration, please follow the link from your email again.'))
  );
  $form['selfmap_fieldset']['regonline_reports_self_reg'] = array(
    '#type' => 'textarea',
    '#title' => t('User already mapped'),
    '#description' => t('If the logged in user is already mapped inform him or her that everything is finalized.'),
    '#default_value' => variable_get('regonline_reports_self_reg', t("Your registration is finalized, you're all set for the conference!<br/><br/>Thanks!.")),
  );
  $form['selfmap_fieldset']['regonline_reports_self_fail'] = array(
    '#type' => 'textarea',
    '#title' => t('Registrant already mapped'),
    '#description' => t('If the provided registrant info has already been mapped the user will not be allowed to continue.'),
    '#default_value' => variable_get('regonline_reports_self_fail', t("The provided registrant information is associated with another account.<br/><br/>Please <a href='/contact'>contact us</a> for support.")),
  );
  $form['selfmap_fieldset']['regonline_reports_self_success'] = array(
    '#type' => 'textarea',
    '#title' => t('Successful map'),
    '#description' => t('Text for if the map was successful and the user registration process is complete.'),
    '#default_value' => variable_get('regonline_reports_self_success', t('Success, your registration is finalized!')),
  );
  
  return system_settings_form($form);
}