<?php
// $Id$

/**
 * Implementation of hook_drush_command().
 */
function regonline_reports_drush_command() {
  return array(
    'reg-collect' => array(
      'alias' => array('regs'),
      'callback' => '_regonline_reports_drush_collect',
      'description' => dt('Collection registrants from RegOnline'),
    ),
    /*'reg-test' => array(
      'callback' => '_regonline_reports_drush_test',
    ),*/
  );
}

function _regonline_reports_drush_collect() {
  regonline_reports_collect_registrants();
}

function _regonline_reports_drush_test() {
  $ignored_registrants = variable_get('regonline_reports_ignore', '');
    $ignored_registrants = array_map('trim', explode("\n", $ignored_registrants));
    var_dump($ignored_registrants);
  return;
  $account_id = variable_get('regonline_account_id', '');
  $username = variable_get('regonline_account_username', '');
  $password = variable_get('regonline_account_password', '');

  $event_id = variable_get('regonline_id', 911915);
  $report_id = variable_get('regonline_report_id', 535753);

  module_load_include('class.inc', 'regonline_api');
  $regonline = new RegOnline($username, $password, $account_id);
  $regonline->setLogLevel(WATCHDOG_DEBUG);
  $xml = $regonline->getReport($report_id, $event_id);

  if ($xml) {
    $registrants = $associated_registrants = array();
    $count = $mapped = 0;
    foreach ($xml->Table1 as $registrant) {
      // Transform SimpleXML object to a standard object.
      $registrant = _regonline_reports_xml_transform($registrant);
      // Save registrant.
      //if (regonline_reports_save_registrant($registrant) != FALSE) {
        //$count++;
     // }
      // Try and map registrant.
      //$map = regonline_reports_map_registrant($registrant);
      //if ($map) {
        //$mapped++;
        // Save map to registrant.
        //$registrant->map = $map;
        //$associated_registrants[] = $registrant;
      //}
      print $registrant->firstlastname . ' ' . $registrant->statusdescription . "\n";
      //$registrants[] = $registrant;
    }
    //drush_print(dt('!count of registrants', array('!count' => count($registrants))));
  }
}
