<?php
//$Id$

/**
 * Implementation of hook_views_default_views().
 */
function regonline_reports_views_default_views() {
  /*
   * View 'regonline_mapped_registrants'
   */
  $view = new view;
  $view->name = 'regonline_mapped_registrants';
  $view->description = 'Registrations with associated accounts';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'regonline_registrants_map';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'regid' => array(
      'label' => 'regonline_registrants.regid -> regonline_registrants_map.regid',
      'required' => 0,
      'id' => 'regid',
      'table' => 'regonline_registrants',
      'field' => 'regid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('fields', array(
    'firstlastname' => array(
      'label' => 'Name',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'firstlastname',
      'table' => 'regonline_registrants',
      'field' => 'firstlastname',
      'relationship' => 'none',
    ),
    'email_address' => array(
      'label' => 'Email address',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'email_address',
      'table' => 'regonline_registrants',
      'field' => 'email_address',
      'relationship' => 'none',
    ),
    'regid' => array(
      'label' => 'RegId',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => '',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 0,
      'id' => 'regid',
      'table' => 'regonline_registrants',
      'field' => 'regid',
      'relationship' => 'none',
    ),
    'regdate' => array(
      'label' => 'Registration date',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'date_format' => 'small',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'regdate',
      'table' => 'regonline_registrants',
      'field' => 'regdate',
      'relationship' => 'none',
    ),
    'regtypedescription' => array(
      'label' => 'Registration type',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'regtypedescription',
      'table' => 'regonline_registrants',
      'field' => 'regtypedescription',
      'relationship' => 'none',
    ),
    'statusdescription' => array(
      'label' => 'Status',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'statusdescription',
      'table' => 'regonline_registrants',
      'field' => 'statusdescription',
      'relationship' => 'none',
    ),
    'duid' => array(
      'label' => 'drupal.org UID',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<a href="http://drupal.org/user/[duid]">[duid]</a>',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => '',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 0,
      'id' => 'duid',
      'table' => 'regonline_registrants_map',
      'field' => 'duid',
      'relationship' => 'none',
    ),
    'uid' => array(
      'label' => 'chicago UID',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<a href="/user/[uid]">[uid]</a>',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => '',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 0,
      'id' => 'uid',
      'table' => 'regonline_registrants_map',
      'field' => 'uid',
      'relationship' => 'none',
    ),
    'nothing' => array(
      'label' => 'Edit',
      'alter' => array(
        'text' => '<a href="/admin/reports/regonline/edit-map/[regid]">edit map</a>',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'nothing',
      'table' => 'views',
      'field' => 'nothing',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Registrants with associated Drupal accounts');
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'desc',
    'columns' => array(
      'email_address' => 'email_address',
      'firstlastname' => 'firstlastname',
      'groupid' => 'groupid',
      'regid' => 'regid',
      'regdate' => 'regdate',
      'regtypedescription' => 'regtypedescription',
      'statusdescription' => 'statusdescription',
      'duid' => 'duid',
      'uid' => 'uid',
    ),
    'info' => array(
      'email_address' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'firstlastname' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'groupid' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'regid' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'regdate' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'regtypedescription' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'statusdescription' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'duid' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'uid' => array(
        'sortable' => 0,
        'separator' => '',
      ),
    ),
    'default' => 'regdate',
  ));
  $views[$view->name] = $view;

  /*
   * View 'regonline_unmapped_registrants'
   */
  $view = new view;
  $view->name = 'regonline_unmapped_registrants';
  $view->description = 'Registrations not mapped to an account';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'regonline_registrants';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'regid' => array(
      'label' => 'regonline_registrants.regid -> regonline_registrants_map.regid',
      'required' => 0,
      'id' => 'regid',
      'table' => 'regonline_registrants',
      'field' => 'regid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('fields', array(
    'firstlastname' => array(
      'label' => 'Name',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'firstlastname',
      'table' => 'regonline_registrants',
      'field' => 'firstlastname',
      'relationship' => 'none',
    ),
    'regid' => array(
      'label' => 'RegId',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => '',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 0,
      'id' => 'regid',
      'table' => 'regonline_registrants',
      'field' => 'regid',
      'relationship' => 'none',
    ),
    'regid_1' => array(
      'label' => 'Regid',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => '',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 1,
      'id' => 'regid_1',
      'table' => 'regonline_registrants_map',
      'field' => 'regid',
      'relationship' => 'regid',
    ),
    'email_address' => array(
      'label' => 'Email address',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'email_address',
      'table' => 'regonline_registrants',
      'field' => 'email_address',
      'relationship' => 'none',
    ),
    'regdate' => array(
      'label' => 'Registration date',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'date_format' => 'small',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'regdate',
      'table' => 'regonline_registrants',
      'field' => 'regdate',
      'relationship' => 'none',
    ),
    'regtypedescription' => array(
      'label' => 'Registration type',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'regtypedescription',
      'table' => 'regonline_registrants',
      'field' => 'regtypedescription',
      'relationship' => 'none',
    ),
    'statusdescription' => array(
      'label' => 'Status',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'statusdescription',
      'table' => 'regonline_registrants',
      'field' => 'statusdescription',
      'relationship' => 'none',
    ),
    'nothing' => array(
      'label' => 'Add map',
      'alter' => array(
        'text' => '<a href="/admin/reports/regonline/map/?regid=[regid]">map</a>',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'nothing',
      'table' => 'views',
      'field' => 'nothing',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'regid' => array(
      'operator' => 'empty',
      'value' => array(
        'value' => '0',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'regid',
      'table' => 'regonline_registrants_map',
      'field' => 'regid',
      'relationship' => 'regid',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('items_per_page', 20);
  $handler->override_option('offset', 1);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'firstlastname' => 'firstlastname',
      'regid' => 'regid',
      'regid_1' => 'regid_1',
    ),
    'info' => array(
      'firstlastname' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'regid' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'regid_1' => array(
        'sortable' => 0,
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $views[$view->name] = $view;

  return $views;
}
